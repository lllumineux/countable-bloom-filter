import re

from config import Main1Config, Main2Config
from utils.filters import OptimalCountableBloomFilter
from utils.parsers import parse_alexandria_glossary


def main1():
    optimal_cbf = OptimalCountableBloomFilter(
        Main1Config.AVERAGE_WORDS_PER_ARTICLE * Main1Config.ARTICLES_NUMBER,
        Main1Config.FP_PROBABILITY
    )

    for content in parse_alexandria_glossary():
        for word in re.findall(r'\w+', content):
            optimal_cbf.add(word)

    while True:
        if optimal_cbf.check(input('Enter word to check: ')):
            print('\033[92mWord in text\033[0m')
        else:
            print('\033[91mWord not in text\033[0m')


def main2():
    optimal_cbf = OptimalCountableBloomFilter(
        Main2Config.AVERAGE_WORDS_PER_ARTICLE * Main2Config.ARTICLES_NUMBER,
        Main2Config.FP_PROBABILITY
    )

    for word in re.findall(r'\w+', Main2Config.TEXT):
        optimal_cbf.add(word)

    print(f'Number of hash funcs: {len(optimal_cbf.functions)}')
    print(f'Bits array length: {len(optimal_cbf.bits)}')
    print(f'Bits array: {optimal_cbf.bits}')


if __name__ == '__main__':
    # main1()
    main2()
