import random
import re
import time
from typing import Iterable

import requests as requests
from bs4 import BeautifulSoup

from config import Main1Config


def remove_html_tags(text):
    clean = re.compile(r'<.*?>')
    return re.sub(clean, '', text)


def parse_alexandria_glossary() -> Iterable:
    base_url = 'https://coinmarketcap.com'
    soup = BeautifulSoup(requests.get(f'{base_url}/alexandria/glossary').text, 'html.parser')
    links_txt = '\n'.join([str(el) for el in soup.find_all('a')])
    links = [f'{base_url}/{el[7:-1]}' for el in re.findall(r'href="/alexandria/glossary/\S*"', links_txt)]
    for _ in range(Main1Config.ARTICLES_NUMBER):
        link = random.choice(links)
        print(f'Parse: {link}')
        time.sleep(5)
        soup = BeautifulSoup(requests.get(link).text, 'html.parser')
        text_with_html_tags = soup.find('div', {'class': 'sc-bdfBwQ Container-sc-4c5vqs-0 dCEoLC khdEkh'}).text
        yield remove_html_tags(text_with_html_tags)
