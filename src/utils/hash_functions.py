import math
import random


class CustomHashFunction:
    def __init__(self, seed: int = None):
        self.seed = seed if seed else math.floor(random.random() * 32) + 32

    def __call__(self, word: str) -> int:
        result = 1
        for sym in word:
            result = (self.seed * result + ord(sym)) & 0xffffffff
        return result
