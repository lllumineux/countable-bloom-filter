import math

from utils.hash_functions import CustomHashFunction


class BloomFilter:
    def __init__(self, size: int, functions: list):
        self.size = size
        self.bits = [0] * self.size
        self.functions = functions

    def add(self, word: str) -> None:
        for func in self.functions:
            self.bits[func(word) % self.size] = 1

    def check(self, word: str) -> bool:
        for func in self.functions:
            if self.bits[func(word) % self.size] == 0:
                return False
            return True


class OptimalBloomFilter(BloomFilter):
    def __init__(self, number_of_elements: int, fp_probability: float):
        size = math.ceil((number_of_elements * math.log(fp_probability)) / math.log(2) ** 2)
        number_of_functions = math.ceil((size / number_of_elements) * math.log(2))
        super().__init__(size, [CustomHashFunction() for _ in range(number_of_functions)])


class CountableBloomFilter(BloomFilter):
    def __init__(self, size: int, functions: list):
        super().__init__(size, functions)

    def add(self, word: str) -> None:
        for func in self.functions:
            self.bits[func(word) % self.size] += 1

    def delete(self, word: str) -> None:
        for func in self.functions:
            self.bits[func(word) % self.size] -= 1


class OptimalCountableBloomFilter(CountableBloomFilter, OptimalBloomFilter):
    def __init__(self, number_of_elements: int, fp_probability: float):
        super().__init__(number_of_elements, fp_probability)
